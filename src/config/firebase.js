import Firebase from "firebase"

const config = {
    apiKey: "AIzaSyB_NeFCNSYXic4bBwzbml_ZnzDWiE7cgTI",
    authDomain: "vue-chat-application.firebaseapp.com",
    databaseURL: "https://vue-chat-application.firebaseio.com",
    storageBucket: "vue-chat-application"
};

export default Firebase.initializeApp(config);