import Vue from "vue"
import Router from "vue-router"
import VueFire from "vuefire"

import App from "./components/App.vue"
import Panel from "./components/Panel.vue"
import Room from "./components/Room.vue"

Vue.use(Router);
Vue.use(VueFire);

var router = new Router({
  history: false,
  root: '/'
});

router.map({
  "*": {
    component: Room
  },
  "/chat/:room": {
    component: Panel
  }
});

router.start(App, 'main');